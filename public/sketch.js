let socket;
var canvas, 
		ctx, 
		flag = false, 
		prevX = 0, 
		currX = 0, 
		prevY = 0, 
		currY = 0, 
		dot_flag = false;
var color = "black", 
		size = 8;

function selectColor(obj) {
	$('.color-choice').each(function(){
		$(this).removeClass('active-border')
	});
	switch (obj.id) {
		case "red":
			color = "red";
			break;
		case "orange":
			color = "#f25f25";
			break;
		case "yellow":
			color = "#fff22f";
			break;
		case "green":
			color = "#0A6757";
			break;
		case "blue":
			color = "#4D83C3";
			break;
		case "indigo":
			color = "#32449A";
			break;
		case "pink":
			color = "#FF69B4";
			break;
		case "violet":
			color = "#CD853F";
			break;
		case "brown":
			color = "#8B4513";
			break;
		case "grey":
			color = "#808080";
			break;
		case "black":
			color = "black";
			break;
		case "white":
			color = "white";
			break;
	}

	$(obj).addClass('active-border');
}

function selectSize(obj) {
	$('.brush-choice').each(function(){
		$(this).removeClass('active-border')
	});
	switch (obj.id) {
		case "small":
			size = 8;
			break;
		case "medium":
			size = 14;
			break;
		case "large":
			size = 20;
			break;
		case "xl":
			size = 30;
			break;
	}

	$(obj).addClass('active-border');
}

function draw() {
	ctx.beginPath();
	ctx.moveTo(prevX, prevY);
	ctx.lineTo(currX, currY);
	ctx.strokeStyle = color;
	ctx.lineWidth = size;
	ctx.lineJoin = 'round';
	ctx.lineCap = 'round';
	ctx.stroke();
	ctx.closePath();
	sendMouse(currX, currY, prevX, prevY, color, size);
}

function save() {
	ctx.clearRect(0, 0, w, h);
	clearCanvas();
}

function clearCanvas(){
	w = canvas.width;
	h = canvas.height;
	ctx.clearRect(0, 0, w, h);
	socket.emit('clear');
}

function findxy(res, e) {
	if (res == 'down') {
		prevX = currX;
		prevY = currY;
		currX = e.clientX - canvas.offsetLeft;
		currY = e.clientY - canvas.offsetTop;
		flag = true;
		dot_flag = true;
		if (dot_flag) {
			ctx.beginPath();
			ctx.fillStyle = color;
			ctx.fillRect(currX, currY, 2, 2);
			ctx.closePath();
			dot_flag = false;
		}
	}
	if (res == 'up' || res == "out") {
		flag = false;
	}
	if (res == 'move') {
		if (flag) {
			prevX = currX;
			prevY = currY;
			currX = e.clientX - canvas.offsetLeft;
			currY = e.clientY - canvas.offsetTop;
			draw();
		}
	}
}

// non-canvas scripts
var words = ['quicksand', 'jellyfish', 'arch', 'wax', 'committee', 'woman', 'zebra', 'vessel', 'body', 'robin', 'ball', 'manager', 'powder', 'treatment', 'box', 'key', 'pen', 'bubble', 'story', 'goldfish', 'needle', 'trains', 'wilderness', 'beam', 'knife', 'wealth', 'stone', 'angle', 'money', 'potato', 'knot', 'bat', 'pancake', 'car', 'cat', 'show', 'drum', 'skin', 'lace', 'maid', 'street', 'pain', 'shade', 'boats', 'daughter', 'chain', 'beetle', 'button', 'route', 'balloon', 'wrench', 'crow', 'joke', 'letter', 'heat', 'war', 'brother', 'scissors', 'company', 'cabbage', 'appliance', 'women', 'worm', 'father', 'smash', 'can', 'swim', 'pencil', 'coat', 'heart', 'look', 'verse', 'cause', 'nation', 'design', 'insect', 'flower', 'ant', 'sticks', 'bike', 'ear', 'reward', 'cap', 'machine', 'blade', 'leaf', 'visitor', 'bath', 'twist', 'bubble', 'stage', 'tank', 'guide', 'jail', 'letters', 'bears', 'writing', 'sheep', 'ice', 'road', 'division', 'dust', 'sponge', 'chalk', 'bushes', 'rat', 'sticks', 'mom', 'wash', 'bear', 'seed', 'turkey', 'crime', 'seat', 'rhythm', 'trail', 'giraffe', 'stretch', 'spiders', 'basketball', 'sugar', 'elbow', 'rabbit', 'cart', 'drug', 'owl', 'lace', 'fish', 'connection', 'trouble', 'earthquake', 'loaf', 'umbrella', 'sock', 'parcel', 'cherries', 'cattle', 'toothpaste', 'burn', 'hair', 'discovery', 'muscle'];

var adjectives = [ 'abrupt', 'acidic', 'adorable', 'adventurous', 'aggressive', 'agitated', 'alert', 'aloof', 'amiable', 'amused', 'annoyed', 'antsy', 'anxious', 'appalling', 'appetizing', 'apprehensive', 'arrogant', 'ashamed', 'astonishing', 'attractive', 'average', 'batty', 'beefy', 'bewildered', 'biting', 'bitter', 'bland', 'blushing', 'bored', 'brave', 'bright', 'broad', 'bulky', 'burly', 'charming', 'cheeky', 'cheerful', 'chubby', 'clean', 'clear', 'cloudy', 'clueless', 'clumsy', 'colorful', 'colossal', 'combative', 'comfortable', 'condemned', 'condescending', 'confused', 'contemplative', 'convincing', 'convoluted', 'cooperative', 'corny', 'costly', 'courageous', 'crabby', 'creepy', 'crooked', 'cruel', 'cumbersome', 'curved', 'cynical', 'dangerous', 'dashing', 'decayed', 'deceitful', 'deep', 'defeated', 'defiant', 'delicious', 'delightful', 'depraved', 'depressed', 'despicable', 'determined', 'dilapidated', 'diminutive', 'disgusted', 'distinct', 'distraught', 'distressed', 'disturbed', 'dizzy', 'drab', 'drained', 'dull', 'eager', 'ecstatic', 'elated', 'elegant', 'emaciated', 'embarrassed', 'enchanting', 'encouraging', 'energetic', 'enormous', 'enthusiastic', 'envious', 'exasperated', 'excited', 'exhilarated', 'extensive', 'exuberant', 'fancy', 'fantastic', 'fierce ', 'filthy', 'flat', 'floppy', 'fluttering', 'foolish', 'frantic', 'fresh', 'friendly', 'frightened', 'frothy', 'frustrating', 'funny', 'fuzzy', 'gaudy', 'gentle', 'ghastly', 'giddy', 'gigantic', 'glamorous', 'gleaming', 'glorious', 'gorgeous', 'graceful', 'greasy', 'grieving', 'gritty', 'grotesque', 'grubby', 'grumpy', 'handsome', 'happy', 'harebrained', 'healthy', 'helpful', 'helpless', 'high', 'hollow', 'homely', 'horrific', 'huge', 'hungry', 'hurt', 'icy', 'ideal', 'immense', 'impressionable', 'intrigued', 'irate', 'irritable', 'itchy', 'jealous', 'jittery', 'jolly', 'joyous', 'filthy', 'flat', 'floppy', 'fluttering', 'foolish', 'frantic', 'fresh', 'friendly', 'frightened', 'frothy', 'frustrating', 'funny', 'fuzzy', 'gaudy', 'gentle', 'ghastly', 'giddy', 'gigantic', 'glamorous', 'gleaming', 'glorious', 'gorgeous', 'graceful', 'greasy', 'grieving', 'gritty', 'grotesque', 'grubby', 'grumpy', 'handsome', 'happy', 'harebrained', 'healthy', 'helpful', 'helpless', 'high', 'hollow', 'homely', 'horrific', 'huge', 'hungry', 'hurt', 'icy', 'ideal', 'immense', 'impressionable', 'intrigued', 'irate', 'irritable', 'itchy', 'jealous', 'jittery', 'jolly', 'joyous ', 'juicy', 'jumpy', 'kind', 'lackadaisical', 'large', 'lazy', 'lethal', 'little', 'lively', 'livid', 'lonely', 'loose', 'lovely', 'lucky', 'ludicrous', 'macho', 'magnificent', 'mammoth', 'maniacal', 'massive', 'melancholy', 'melted', 'miniature', 'minute', 'mistaken', 'misty', 'moody', 'mortified', 'motionless', 'muddy', 'mysterious', 'narrow', 'nasty', 'naughty', 'nervous', 'nonchalant', 'nonsensical', 'nutritious', 'nutty', 'obedient', 'oblivious', 'obnoxious', 'odd', 'old-fashioned', 'outrageous', 'panicky', 'perfect', 'perplexed', 'petite', 'petty', 'plain', 'pleasant', 'poised', 'pompous', 'precious', 'prickly', 'proud', 'pungent', 'puny', 'quaint', 'quizzical', 'ratty', 'reassured', 'relieved', 'repulsive', 'responsive', 'ripe', 'robust', 'rotten', 'rotund', 'rough', 'round', 'salty', 'sarcastic', 'scant', 'scary', 'scattered', 'scrawny', 'selfish', 'shaggy', 'shaky', 'shallow', 'sharp', 'shiny', 'short', 'silky', 'silly', 'skinny', 'slimy', 'slippery', 'small', 'smarmy', 'smiling', 'smoggy', 'smooth', 'smug', 'soggy', 'solid', 'sore', 'sour', 'sparkling', 'spicy', 'splendid', 'spotless', 'square', 'stale', 'steady', 'steep ', 'sticky', 'stormy', 'stout', 'straight', 'strange', 'strong', 'stunning', 'substantial', 'successful', 'succulent', 'superficial', 'superior', 'swanky', 'sweet', 'tart', 'tasty', 'teeny', 'tender', 'tense', 'terrible', 'testy', 'thankful', 'thick', 'thoughtful', 'thoughtless', 'tight', 'timely', 'tricky', 'trite', 'troubled', 'uneven', 'unsightly', 'upset', 'uptight', 'vast', 'vexed', 'victorious', 'virtuous', 'vivacious', 'vivid', 'wacky', 'weary', 'whimsical', 'whopping', 'wicked', 'witty', 'wobbly', 'wonderful', 'worried', 'yummy', 'zany', 'zealous', 'zippy'];

var wordChoices;
var selectedWordWrapper = $('#selected-word-wrapper');
var adjective;
var counter;
var count;

function randomWord(){
	wordChoices = [words[Math.floor(Math.random()*words.length)], words[Math.floor(Math.random()*words.length)]];
	console.log(wordChoices[0]);
	$('.word-choice-wrapper').html('<div><h2>Choose a word</h2><span class="word-choice" id="word1" onclick="selectWord(this)">'+wordChoices[0]+'</span><span class="word-choice" id="word2" onclick="selectWord(this)">'+wordChoices[1]+'</span></div>');
	$(selectedWordWrapper).css('top', -250);
	$('.word-choice-wrapper').fadeIn('fast');
}

function randomAdjective(){
	adjective = adjectives[Math.floor(Math.random()*adjectives.length)];
	console.log(adjective)
	return adjective;
}


function selectWord(word){
	$selectedWord = $(word).text();
	console.log($selectedWord);
	$('#selected-word-wrapper span').text($selectedWord);
	$('#selected-word-wrapper').css('top', 0);
	$('.word-choice-wrapper').fadeOut('fast');
	counter=setInterval(startTimer, 1000);
	count=60;
	// startTimer();
}


// Sending data to the socket
function emitAndCanvas(){ 
	var dataURL = canvas.toDataURL();
	document.getElementById("previous-image").src = dataURL;
	document.getElementById("previous-image").style.display = "inline";
	$('.img-label').css('display', 'inline-block');

	socket.emit('clear', dataURL);
	clearCanvas();
	randomWord();
}

function clearCanvas() {
	ctx.clearRect(0, 0, canvas.width, canvas.height); 
}

function sendMouse(x, y, pX, pY, color, size) {
	const data = {
		x: x,
		y: y,
		px: pX,
		py: pY,
		color: color,
		size: size,
	}
	socket.emit('mouse', data)
}

// set canvas
$(function() {
	canvas = document.getElementById('can');
	ctx = canvas.getContext("2d");
	w = 430;
	h = 540;
	canvas.setAttribute('width', w);
	canvas.setAttribute('height', h);

	canvas.addEventListener("mousemove", function (e) {
			findxy('move', e)
	}, false);
	canvas.addEventListener("mousedown", function (e) {
			findxy('down', e)
	}, false);
	canvas.addEventListener("mouseup", function (e) {
			findxy('up', e)
	}, false);
	canvas.addEventListener("mouseout", function (e) {
			findxy('out', e)
	}, false);

});

$(window).ready(function(){
	randomWord();
});

// Start the socket connection
socket = io.connect('http://localhost:4000')

// Callback function
socket.on('mouse', data => {
	ctx.beginPath();
	ctx.moveTo(data.px, data.py);
	ctx.lineTo(data.x, data.y);
	ctx.strokeStyle = data.color;
	ctx.lineWidth = data.size;
	ctx.lineJoin = 'round';
	ctx.lineCap = 'round';
	ctx.stroke();
	ctx.closePath();
})

socket.on('clear', data => {
	ctx.clearRect(0, 0, canvas.width, canvas.height); 
	document.getElementById("previous-image").src = data;
	document.getElementById("previous-image").style.display = "inline";
	$('.img-label').css('display', 'inline-block');
})